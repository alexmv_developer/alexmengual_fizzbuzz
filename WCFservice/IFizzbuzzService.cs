﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFservice
{
    [ServiceContract]
    public interface IFizzbuzzService
    {
        [OperationContract]
        List<string> Fizzbuzz(int num);
    }
}
