﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace WCFservice
{
    public class FizzbuzzService : IFizzbuzzService
    {
        List<string> listFizzbuzz = new List<string>();

        //Si aqui pongo el limit hardcoded por ejemplo: limit = 20;
        //el test si que pasa, pero con esto no, aunque el programa funciona igual
        //y no he podido encontrar el por qué.
        int limit = Int32.Parse(ConfigurationManager.AppSettings["limit"]);

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<string> Fizzbuzz(int num)
        {
            log.Info("Start method Fizzbuzz");
            try
            {

                if (num > limit)
                {
                    log.Error("The number is bigger than the limit");
                    throw new MyCustomException("The number is bigger than the limit");
                }

                for (int i = num; i < limit; i++)
                {
                    if (i % 3 == 0 && i % 5 == 0)
                    {
                        listFizzbuzz.Add("fizzbuzz");
                    }
                    else if (i % 5 == 0)
                    {
                        listFizzbuzz.Add("buzz");
                    }
                    else if (i % 3 == 0)
                    {
                        listFizzbuzz.Add("fizz");
                    }
                    else
                    {
                        listFizzbuzz.Add(i.ToString());
                    }
                }

                SaveFile(listFizzbuzz);
            }
            catch(Exception e)
            {
                log.Error(e.Message);
            }

            log.Info("End method Fizzbuzz");

            return listFizzbuzz;
        }

        public void SaveFile(List<string> listFizzbuzz)
        {

            log.Info("Start method SaveFile");
            if (listFizzbuzz == null)
            {
                log.Error("List is null");
                throw new MyCustomException("List cannot be null");
            }

            try
            {
                string path = "C:/Users/Alex/Desktop/";
                string filename = "results.txt";

                TextWriter file = new StreamWriter(path + filename);
                string date = DateTime.Now.ToString("G");

                file.WriteLine(String.Join(", ", listFizzbuzz) + ", " + date);
                file.Close();
            }
            catch(Exception e)
            {
                log.Error(e.Message);
            }

            log.Info("End method SaveFile");
        }
    }
}
