﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace WCFservice
{
    public class MyAppConfig
    {
        public static void AddValue(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove(key);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            config.AppSettings.Settings.Add(key, value);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static string GetValue(string key, string defaultt)
        {
            string myreturn = ConfigurationManager.AppSettings[key];
            if (myreturn == null)
            {
                myreturn = defaultt;
            }

            return myreturn;
        }
    }
}