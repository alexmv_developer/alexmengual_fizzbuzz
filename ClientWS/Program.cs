using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientWS
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                int num = Int32.Parse(Console.ReadLine());

                string[] list;

                using (FizzbuzzService.FizzbuzzServiceClient client = new FizzbuzzService.FizzbuzzServiceClient())
                {
                    list = client.Fizzbuzz(num);

                    foreach (string l in list)
                    {
                        Console.WriteLine(l);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
