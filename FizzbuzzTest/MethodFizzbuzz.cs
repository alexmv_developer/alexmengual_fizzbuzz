﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WCFservice;

namespace FizzbuzzTest
{
    [TestClass]
    public class MethodFizzbuzz
    {
        [TestMethod]
        public void FizzbuzzTest()
        {
            var num = 15;
            List<string> list = new List<string>();
            list.Add("fizzbuzz");
            list.Add("16");
            list.Add("17");
            list.Add("fizz");
            list.Add("19");

            var calculator = new FizzbuzzService();
            var result = calculator.Fizzbuzz(num);

            CollectionAssert.AreEquivalent(list, result);
        }
    }
}
